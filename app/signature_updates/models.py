# -*- coding: utf-8 -*-
from django.db import models


class Signature(models.Model):
    docfile = models.FileField(upload_to='signatures/%Y/%m/%d')
