# -*- coding: utf-8 -*-
from django.shortcuts import render
from django.template import RequestContext
from django.http import HttpResponseRedirect
from django.core.urlresolvers import reverse

from .models import Signature
from .forms import SignatureForm


def uploadsignature(request):
    # Handle file upload
    if request.method == 'POST':
        form = SignatureForm(request.POST, request.FILES)
        if form.is_valid():
            newdoc = Signature(docfile=request.FILES['docfile'])
            newdoc.save()

            # Redirect to the document list after POST
            return HttpResponseRedirect(reverse('uploadsignature'))
    else:
        form = SignatureForm()  # A empty, unbound form
        
    # Load documents for the list page
    signatures = Signature.objects.all()

    # Render list page with the documents and the form
    return render(
        request,
        'uploadsignature.html',
        {'signatures': signatures, 'form': form}
    )
