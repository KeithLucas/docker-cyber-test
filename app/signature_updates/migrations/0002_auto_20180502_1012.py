# -*- coding: utf-8 -*-
# Generated by Django 1.9 on 2018-05-02 10:12
from __future__ import unicode_literals

from django.db import migrations


class Migration(migrations.Migration):

    dependencies = [
        ('signature_updates', '0001_initial'),
    ]

    operations = [
        migrations.RenameModel(
            old_name='Document',
            new_name='Signature',
        ),
    ]
