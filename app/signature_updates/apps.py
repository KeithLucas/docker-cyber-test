from django.apps import AppConfig


class SignatureUpdatesConfig(AppConfig):
    name = 'signature_updates'
