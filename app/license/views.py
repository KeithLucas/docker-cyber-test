from django.http import HttpResponseRedirect
import random
from datetime import datetime, timedelta
from license.models import *
from license.key_generator import generate_key

charlist = [x for x in 'ABCDEFGHJKLMNPQRSTUVWXYZ23456789']

def AddKeysView(request):
    for i in range(10):
        print(i,generate_key())
        
        all_keys = LicenseKey.objects.all().order_by("-id")
        key_id = 1
        
        if all_keys:
            getid = all_keys[0].id
            key_id = getid + 1
            
        new_key = LicenseKey(
            id=key_id,
            license_key = generate_key(),
            expiry_date = (datetime.now() + timedelta(days=30)),
            license_status = "Available"
        )
        new_key.save()
        
    return HttpResponseRedirect("/keysadded")