from datetime import datetime, timedelta
import binascii
import os

from django.db import models
from django.utils import timezone
from django.utils.encoding import python_2_unicode_compatible
from django.utils.translation import ugettext_lazy as _

import mongoengine
from mongoengine.django import auth
from mongoengine import fields, Document, ImproperlyConfigured

class LicenseKey(Document):
    
    id = fields.IntField(primary_key=True)
    license_key = fields.StringField(required=True)
    created_date = fields.DateTimeField(default=timezone.now, verbose_name=_('date created'))
    expiry_date = fields.DateTimeField()
    license_status = fields.StringField()
    
    REQUIRED_FIELDS = ['license_key']
    
    @classmethod
    def create_key(cls, license_key, expiry_date, license_status):
        """
        Create (and save) a new key entry in DB
        """
        now = datetime.now()
        entry = cls(license_key=license_key, expiry_date=expiry_date, license_status=license_status, created_date=now)
        entry.save()
        return entry
        
    def set_status(self, status):
        
        self.license_status = status
        self.save()
        return self