'''
from Crypto.Cipher import AES
import base64
from django.utils.crypto import get_random_string

chars = 'ABCDEFGHJKLMNPQRSTUVWXYZ23456789'
license = get_random_string(32, chars)
msg_text = 'test some plain text here'.rjust(32)
#secret_key = '1234567890123456' # create new & store somewhere safe

cipher = AES.new('XunisonXbrainSecurityIND',AES.MODE_ECB) # never use ECB in strong systems obviously
encoded = base64.b64encode(cipher.encrypt(license))

str1 = ''.join(ch for ch in encoded if ch.isalnum())

key = ''
for ch in str1:
    if ch.islower():  key += ch.upper()
    else:       key += ch


key2 = [c for c in key if c not in ['1','0','I','O']]

key3 = "".join(list(set(key2)))

print(len(key2),"".join(key2),len(key3), key3)


'''
import random

from license.models import *

charlist = [x for x in 'ABCDEFGHJKLMNPQRSTUVWXYZ23456789']

def generate_key():

    sample = "".join(random.sample(charlist, 25))
    key = "-".join([sample[:5], sample[5:10], sample[10:15], sample[15:20], sample[20:]])

    return key


