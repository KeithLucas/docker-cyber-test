import datetime
import binascii
import os

from django.db import models
from django.utils import timezone
from django.utils.encoding import python_2_unicode_compatible
from django.utils.translation import ugettext_lazy as _

import mongoengine
from mongoengine.django import auth
from mongoengine import fields, Document, ImproperlyConfigured

class DeviceConnection(Document):
    
    id = fields.IntField(primary_key=True)
    router_macid = fields.StringField(required=True)
    device_macid = fields.StringField(required=True)
    device_name = fields.StringField(default="Device Name")
    device_type = fields.StringField(default="Device Type")
    username = fields.StringField(default="NAME")
    age = fields.IntField(default=18)
    is_active = fields.IntField(default=1)
    last_connected_on = fields.DateTimeField(default=timezone.now)
    internet_full = fields.IntField(default=1)     # flag 1 if full , 0 if limited access
    internet_from = fields.StringField(default="00:00")              # like 14:00 for 2 PM
    internet_to = fields.StringField(default="23:59")                # like 18:00 for 6 PM
    internet_days = fields.StringField(default='1111111')           # like 1010100' for Sun, Tue, Thu
    blocked_sites = fields.StringField(default="")
    pause = fields.IntField(default=0)
    modified_date = fields.DateTimeField(default=timezone.now)
    created_date = fields.DateTimeField(default=timezone.now, verbose_name=_('date created'))
    
    REQUIRED_FIELDS = ['device_macid', 'router_macid']
    
    @classmethod
    def create_entry(cls,router_macid, device_macid, username, age, is_active, last_connected_on, internet_full, internet_from, internet_to, internet_days,
                        blocked_sites, pause, device_name, device_type):
        """
        Create (and save) a new log with the given details
        """
        now = datetime.datetime.now()
        entry = cls(router_macid=router_macid, device_macid=device_macid,device_name=device_name, device_type=device_type, username=username, age=age, is_active=is_active, last_connected_on=last_connected_on, 
                    internet_full=internet_full, internet_from=internet_from, internet_to=internet_to, internet_days=internet_days,
                        blocked_sites=blocked_sites, pause=pause, modified_date=now, created_date=now)
        entry.save()
        return entry


    def check_entry(self, router_macid, device_macid=None):
        """
        Check if same entry already exist in database
        """
        
        try:
            print(router_macid, device_macid)
            if device_macid: entry = DeviceConnection.objects.filter(device_macid__iexact=device_macid,router_macid__iexact=router_macid)
            else : entry = DeviceConnection.objects.filter(router_macid__iexact=router_macid)
            print(entry)
            if entry : return True
            else : return False
        except:
            return False
        
    def update_entry(self, device_macid, router_macid, attrs):
        """
        Update if same entry already exist in database
        """
        
        try:
            entry = DeviceConnection.objects.get(device_macid__iexact=device_macid, router_macid__iexact=router_macid)
            entry.username = attrs.get("username")
            entry.age = attrs.get('age')
            entry.is_active = attrs.get('is_active')
            entry.last_connected_on = attrs.get('last_connected_on')
            entry.internet_full = attrs.get('internet_full')
            entry.internet_from = attrs.get('internet_from')
            entry.internet_to = attrs.get('internet_to')
            entry.internet_days = attrs.get('internet_days')
            entry.blocked_sites = attrs.get('blocked_sites')
            entry.pause = attrs.get('pause')
            entry.device_type = attrs.get('device_type')
            entry.device_name = attrs.get('device_name')
            entry.modified_date = datetime.datetime.now()
            entry.save()
        except:
            return


class BlockList(Document):
    
    id = fields.IntField(primary_key=True)
    router_macid = fields.StringField(required=True)
    device_macid = fields.StringField(required=True)
    block_ip_domain = fields.StringField()
    block_status = fields.IntField(default=1)
    modified_date = fields.DateTimeField(default=timezone.now)
    created_date =  fields.DateTimeField(default=timezone.now)
    REQUIRED_FIELDS = ['device_macid', 'block_ip_domain']
    
    @classmethod
    def create_entry(cls, router_macid, device_macid, block_ip_domain):
        """
        Create (and save) a new log with the given details
        """
        now = datetime.datetime.now()
        entry = cls(router_macid=router_macid,device_macid=device_macid, block_ip_domain=block_ip_domain,
                    modified_date=now, created_date=now, block_status=1)
        entry.save()
        return entry
        
    def check_entry(self, router_macid, device_macid="", block_ip_domain=""):
        """
        Check if same entry already exist in database
        """
        
        try:
            print(router_macid, device_macid)
            if block_ip_domain: entry = BlockList.objects.filter(router_macid__iexact=router_macid,device_macid__iexact=device_macid,block_ip_domain=block_ip_domain)
            elif device_macid: entry = BlockList.objects.filter(router_macid__iexact=router_macid,device_macid__iexact=device_macid)
            else: entry = BlockList.objects.filter(router_macid__iexact=router_macid)
            return True
        except:
            return False
        
    def update_entry(self,router_macid, device_macid, block_ip_domain, status):
        """
        Update if same entry already exist in database
        """
        
        try:
            entry = BlockList.objects.get(router_macid__iexact=router_macid,device_macid__iexact=device_macid,block_ip_domain=block_ip_domain)
            entry.block_status = status
            entry.modified_date = datetime.datetime.now()
            entry.save()
        except:
            return
            
class Pincode(Document):
    
    router_macid = fields.StringField(required=True)
    router_serial_number = fields.StringField(required=True)
    pincode = fields.StringField(default="000000")
    modified_date = fields.DateTimeField(default=timezone.now)
    created_date =  fields.DateTimeField(default=timezone.now)
    REQUIRED_FIELDS = ['router_macid', 'router_serial_number']
    
    @classmethod
    def create_entry(cls, router_macid, router_serial_number, pincode):
    #def create_entry(cls, router_macid, pincode):
        """
        Create (and save) a new entry with the given details
        """
        now = datetime.datetime.now()
        entry = cls(router_macid=router_macid,router_serial_number=router_serial_number, pincode=000000,
        #entry = cls(router_macid=router_macid, pincode=000000,
                    modified_date=now, created_date=now)
        entry.save()
        return entry
        
    def check_entry(self, router_macid, router_serial_number):
    #def check_entry(self, router_macid):
        """
        Check if same entry already exist in database
        """
        
        try:
            entry = Pincode.objects.filter(router_serial_number__iexact=router_serial_number)
            #entry = Pincode.objects.filter(router_macid__iexact=router_macid)
            return True
        except:
            return False
        
    def update_entry(self,router_macid, router_serial_number, pincode):
    #def update_entry(self,router_macid, pincode):
        """
        Update if same entry already exist in database
        """
        
        try:
            entry = Pincode.objects.get(router_serial_number__iexact=router_serial_number)
            #entry = Pincode.objects.get(router_macid__iexact=router_macid)
            entry.pincode = pincode
            entry.modified_date = datetime.datetime.now()
            entry.save()
        except:
            return
        