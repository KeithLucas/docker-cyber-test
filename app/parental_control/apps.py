from django.apps import AppConfig


class ParentalControlConfig(AppConfig):
    name = 'parental_control'
