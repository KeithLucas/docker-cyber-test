from rest_framework import views, mixins, permissions, exceptions
from rest_framework.response import Response
from rest_framework_mongoengine import viewsets
from rest_framework import parsers, renderers

from .serializers import *
from users.models import *
from .models import *
from users.authentication import TokenAuthentication

from django.core import serializers
from django.http import JsonResponse

import json

class UpdateDeviceConnectionsView(views.APIView):
    serializer_class = UpdateDeviceConnections
    def post(self, request, *args, **kwargs):
        serializer = self.serializer_class(data=request.data)
        log = serializer.is_valid(raise_exception=True)
        status="Connection details of device is updated"
        return Response({'status': status})
        
class AddUpdateDeviceStateView(views.APIView):
    serializer_class = AddUpdateDeviceState
    def post(self, request, *args, **kwargs):
        serializer = self.serializer_class(data=request.data)
        log = serializer.is_valid(raise_exception=True)
        status="Connectivity state of device is added/updated"
        return Response({'status': status})


class GetConnectedDevicesView(views.APIView):
    serializer_class = GetConnectedDevices
    def post(self, request, *args, **kwargs):
        serializer = self.serializer_class(data=request.data)
        log = serializer.is_valid(raise_exception=True)
        if log:
            rows = DeviceConnection.objects.filter(router_macid__iexact=serializer.data['router_macid'], is_active=1)
            print(len(rows))
        else:
            rows = "Failed to get data"
        status="Here is the list of devices currently connected to Router"
        return Response({'status': status, 'connected_devices': json.loads(rows.to_json()) })
        

class AddUpdateBlockListView(views.APIView):
    serializer_class = AddUpdateBlockList
    def post(self, request, *args, **kwargs):
        serializer = self.serializer_class(data=request.data)
        log = serializer.is_valid(raise_exception=True)
        status="Your entry is saved"
        return Response({'status': status})
        

class GetDeviceBlockListView(views.APIView):
    serializer_class = GetDeviceBlockList
    def post(self, request, *args, **kwargs):
        serializer = self.serializer_class(data=request.data)
        log = serializer.is_valid(raise_exception=True)
        if log:
            rows = BlockList.objects.filter(router_macid__iexact=serializer.data['router_macid'],
                                            block_status=1).only('device_macid', 'block_ip_domain')
            blocklist = {}
            for row in rows:
              if row.device_macid in blocklist:
                  blocklist[row.device_macid]['blocked_sites'].append(row.block_ip_domain)
              else:
                  blocklist[row.device_macid] = {}
                  blocklist[row.device_macid]['blocked_sites'] = [row.block_ip_domain]
            
            for device in blocklist:
                entry = DeviceConnection.objects.get(device_macid__iexact=device,router_macid__iexact=serializer.data['router_macid'])
                #if not entry.internet_full:
                blocklist[device]['internet_from'] = entry.internet_from
                blocklist[device]['internet_to'] = entry.internet_to
                blocklist[device]['internet_days'] = entry.internet_days
            
        else:
            rows = "Failed to get data"
        status="Here is the list of blocked IPs and domains"
        return Response({'status': status, 'blocklist': blocklist })
        
class VerifyUpdatePincodeView(views.APIView):
    serializer_class = VerifyUpdatePincode
    def post(self, request, *args, **kwargs):
        serializer = self.serializer_class(data=request.data)
        log = serializer.is_valid(raise_exception=True)
        status="Pin is correct"
        return Response({'status': status})