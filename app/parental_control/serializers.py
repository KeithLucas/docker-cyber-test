from django.contrib.auth import authenticate
from django.utils.translation import ugettext_lazy as _

from rest_framework import serializers
from rest_framework_mongoengine.serializers import DocumentSerializer
from blacklist.models import *
from users.models import *
from parental_control.models import *
from django.conf import settings
import datetime

def validate_token(token):
    try:
        return Token.objects.get(key=token)
    except:
        return

class UpdateDeviceConnections(serializers.Serializer):
    
    device_name=serializers.CharField(label=_('Device name'), default="")
    device_type=serializers.CharField(label=_('Device type'), default="")
    device_macid = serializers.CharField(label=_("Device MAC ID"))
    router_macid = serializers.CharField(label=_("Router MAC ID"))
    username = serializers.CharField()
    age = serializers.IntegerField()
    is_active = serializers.IntegerField()
    last_connected_on = serializers.DateTimeField()
    internet_full = serializers.IntegerField()     # flag 1 if full , 0 if limited access
    internet_from = serializers.CharField()              # like 1400 for 2 PM
    internet_to = serializers.CharField()                # like 1800 for 6 PM
    internet_days = serializers.CharField()           # like 1010100' for Sun, Tue, Thu
    blocked_sites = serializers.CharField(default='')
    pause = serializers.IntegerField()
    modified_date = serializers.DateTimeField()
    token = serializers.CharField(label=_("Token"))
    
    def validate(self, attrs):
        device_macid = attrs.get('device_macid')
        router_macid = attrs.get('router_macid')
        token = attrs.get('token')
        blocked_sites = attrs.get("blocked_sites")
        if router_macid:
            tokendetails = validate_token(token)
            if not tokendetails:
                msg = _('Invalid token.')
                raise serializers.ValidationError(msg)
            else:
                userdetails = tokendetails.user
                if userdetails.check_mac_id(router_macid):
                    row = DeviceConnection()
                    entry = row.check_entry(router_macid=router_macid,device_macid=device_macid)
                    if entry:
                        row.update_entry(router_macid=router_macid,device_macid=device_macid, attrs=attrs)
                        print("saved", blocked_sites)
                        if blocked_sites: updateblocklist(router_macid, device_macid, blocked_sites.split(","))
                    else:
                        msg = _('No entry of such device')
                        raise serializers.ValidationError(msg)
                else:
                    msg = _('Invalid MAC ID')
                    raise serializers.ValidationError(msg)
        else:
            msg = _('Must include "MAC ID".')
            raise serializers.ValidationError(msg)

        attrs['mac_id'] = userdetails.mac_id
        return attrs
        


class AddUpdateDeviceState(serializers.Serializer):

    device_macid = serializers.CharField(label=_("Device MAC ID"))
    router_macid = serializers.CharField(label=_("Router MAC ID"))
    last_connected_on = serializers.DateTimeField()
    is_active = serializers.IntegerField()
    token = serializers.CharField(label=_("Token"))
    
    def validate(self, attrs):
        device_macid = attrs.get('device_macid')
        router_macid = attrs.get('router_macid')
        token = attrs.get('token')
        last_connected_on = attrs.get('last_connected_on')
        is_active = attrs.get('is_active')
        if router_macid:
            tokendetails = validate_token(token)
            if not tokendetails:
                msg = _('Invalid token.')
                raise serializers.ValidationError(msg)
            else:
                userdetails = tokendetails.user
                if userdetails.check_mac_id(router_macid):
                    row = DeviceConnection()
                    entry = row.check_entry(router_macid=router_macid,device_macid=device_macid)
                    if entry:
                        entry = DeviceConnection.objects.get(device_macid__iexact=device_macid, router_macid__iexact=router_macid)
                        if is_active:
                           entry.last_connected_on = last_connected_on
                           entry.is_active = 1
                        else:
                            entry.is_active = 0
                        entry.save()
                    else:
                        all_entries = DeviceConnection.objects.all().order_by("-id")
                        entry_id = 1
                        
                        if all_entries:
                            getid = all_entries[0].id
                            entry_id = getid + 1
                        
                        new_entry = DeviceConnection(
                            id=entry_id,
                            device_name= attrs.get('device_name'),
                            device_type= attrs.get('device_type'),
                            device_macid = attrs.get('device_macid').upper(),
                            router_macid = attrs.get('router_macid').upper(),
                            )
                        
                        new_entry.save()
                else:
                    msg = _('Invalid MAC ID')
                    raise serializers.ValidationError(msg)
        else:
            msg = _('Must include "MAC ID".')
            raise serializers.ValidationError(msg)

        attrs['mac_id'] = userdetails.mac_id
        return attrs
        


class GetConnectedDevices(serializers.Serializer):

    router_macid = serializers.CharField(label=_("Router MAC ID"))
    token = serializers.CharField(label=_("Token"))
    
    def validate(self, attrs):
        router_macid = attrs.get('router_macid')
        token = attrs.get('token')
        if router_macid:
            tokendetails = validate_token(token)
            if not tokendetails:
                msg = _('Invalid token.')
                raise serializers.ValidationError(msg)
            else:
                userdetails = tokendetails.user
                if userdetails.check_mac_id(router_macid):
                    entry = DeviceConnection()
                    if entry.check_entry(router_macid=router_macid):
                       entry = DeviceConnection.objects.filter(router_macid__iexact=router_macid, is_active=1)
                    else:
                        msg = _('No entry of such device')
                        raise serializers.ValidationError(msg)
                else:
                    msg = _('Invalid MAC ID')
                    raise serializers.ValidationError(msg)
        else:
            msg = _('Must include "MAC ID".')
            raise serializers.ValidationError(msg)

        attrs['mac_id'] = userdetails.mac_id
        return attrs



def updateblocklist(router_macid, device_macid, blocklist):
    entry = BlockList()
    if entry.check_entry(router_macid=router_macid,device_macid=device_macid):
        rows = BlockList.objects.filter(router_macid__iexact=router_macid,device_macid__iexact=device_macid)
        for row in rows:
            if row.block_ip_domain in blocklist:
                status = 1
                blocklist.remove(row.block_ip_domain)
                print(blocklist)
            else:
                status = 0
            entry.update_entry(router_macid=router_macid,device_macid=device_macid,block_ip_domain=row.block_ip_domain, status=status)
    
    print("blocklist",blocklist)
    if blocklist:
            
        all_entries = BlockList.objects.all().order_by("-id")
        entry_id = 1
        
        if all_entries:
            getid = all_entries[0].id
            entry_id = getid + 1
            
        for url in blocklist:
            new_entry = BlockList(
                id=entry_id,
                router_macid = router_macid.upper(),
                device_macid = device_macid.upper(),
                block_ip_domain=url,
                
            )
            new_entry.save()
            entry_id += 1
    

class AddUpdateBlockList(serializers.Serializer):
    router_macid = serializers.CharField(label=_("Router MAC ID"))
    device_macid = serializers.CharField(label=_("Device MAC ID"))
    token = serializers.CharField(label=_("Token"))
    block_ip_domain=serializers.CharField(label=_('Blocked IP/Domain name'))
    block_status=serializers.IntegerField(label=_('Blocked status'))
    
    def validate(self, attrs):
        router_macid = attrs.get('router_macid')
        device_macid = attrs.get('device_macid')
        token = attrs.get('token')
        block_ip_domain = attrs.get('block_ip_domain')
        block_status = attrs.get('block_status')
        if router_macid:
            tokendetails = validate_token(token)
            if not tokendetails:
                msg = _('Invalid token.')
                raise serializers.ValidationError(msg)
            else:
                userdetails = tokendetails.user
                if userdetails.check_mac_id(router_macid):
                    updateblocklist(router_macid=router_macid, device_macid=device_macid, blocklist=[block_ip_domain])
                else:
                    msg = _('Invalid MAC ID')
                    raise serializers.ValidationError(msg)
        else:
            msg = _('Must include "MAC ID".')
            raise serializers.ValidationError(msg)

        attrs['mac_id'] = userdetails.mac_id
        return attrs
        
class GetDeviceBlockList(serializers.Serializer):
    router_macid = serializers.CharField(label=_("Router MAC ID"))
    token = serializers.CharField(label=_("Token"))
    
    def validate(self, attrs):
        router_macid = attrs.get('router_macid')
        token = attrs.get('token')
        if router_macid:
            tokendetails = validate_token(token)
            if not tokendetails:
                msg = _('Invalid token.')
                raise serializers.ValidationError(msg)
            else:
                userdetails = tokendetails.user
                if userdetails.check_mac_id(router_macid):
                    entry = BlockList()
                    if not entry.check_entry(router_macid=router_macid):
                        msg = _('No entry for this MAC ID')
                        raise serializers.ValidationError(msg)
                        
                else:
                    msg = _('Invalid MAC ID')
                    raise serializers.ValidationError(msg)
        else:
            msg = _('Must include "MAC ID".')
            raise serializers.ValidationError(msg)

        return attrs
        

class VerifyUpdatePincode(serializers.Serializer):
    router_macid = serializers.CharField(label=_("Router MAC ID"))
    router_serial_number = serializers.CharField(label=_("Router Serial Number"))
    pincode = serializers.CharField(label=_("Pincode"))
    verify = serializers.BooleanField(label=_("Verify?(0/1)"))
    token = serializers.CharField(label=_("Token"))
    
    def validate(self, attrs):
        router_macid = attrs.get('router_macid')
        router_serial_number = attrs.get('router_serial_number')
        pincode = attrs.get('pincode')
        verify = attrs.get("verify")
        token = attrs.get('token')
        if router_macid:
            tokendetails = validate_token(token)
            if not tokendetails:
                msg = _('Invalid token.')
                raise serializers.ValidationError(msg)
            else:
                userdetails = tokendetails.user
                if userdetails.check_mac_id(router_macid):
                    row = Pincode()
                    if row.check_entry(router_macid,router_serial_number):
                    #if row.check_entry(router_macid):
                        if not verify:
                            row.update_entry(router_macid,router_serial_number,pincode)
                            #row.update_entry(router_macid,pincode)
                        else:
                            entry = Pincode.objects.get(router_serial_number__iexact=router_serial_number, router_macid__iexact=router_macid)
                            #entry = Pincode.objects.get(router_macid__iexact=router_macid)
                            if entry.pincode != pincode:
                                msg = _('Incorrect PIN')
                                raise serializers.ValidationError(msg)
                    else:
                        msg = _('No entry in Pincode table')
                        raise serializers.ValidationError(msg)    
                else:
                    msg = _('Invalid MAC ID')
                    raise serializers.ValidationError(msg)
        else:
            msg = _('Must include "MAC ID".')
            raise serializers.ValidationError(msg)

        return attrs