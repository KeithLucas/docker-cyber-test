from rest_framework import views, mixins, permissions, exceptions
from rest_framework.response import Response
from rest_framework_mongoengine import viewsets
from rest_framework import parsers, renderers

from users.serializers import *
from users.models import *
from users.authentication import TokenAuthentication

from django.http import HttpResponseRedirect

class RegistrationView(views.APIView):
    serializer_class = Registration
    def post(self, request, *args, **kwargs):
        serializer = self.serializer_class(data=request.data)
        user = serializer.is_valid(raise_exception=True)
        status="Sucessfully Registered"
        return Response({'status': status})

class ForgetPasswordView(views.APIView):
    serializer_class = ForgetPassword
    def post(self, request, *args, **kwargs):
        serializer = self.serializer_class(data=request.data)
        user = serializer.is_valid(raise_exception=True)
        status="Mail is send to your account."
        return Response({'status': status})

class NewCode(views.APIView):
    serializer_class = UpdateCode
    def post(self, request, *args, **kwargs):
        serializer = self.serializer_class(data=request.data)
        user = serializer.is_valid(raise_exception=True)
        code, created = ForgetPass.objects.get_or_create(user=user)
        status="Your code is updated."
        return Response({'status': status})

class ResetPasswordView(views.APIView):
    serializer_class = ResetPassword
    def post(self, request, *args, **kwargs):
        serializer = self.serializer_class(data=request.data)
        user = serializer.is_valid(raise_exception=True)
        status="Your password is changed."
        return Response({'status': status})

class ChangePasswordView(views.APIView):
    serializer_class = ChangePassword
    def post(self, request, *args, **kwargs):
        serializer = self.serializer_class(data=request.data)
        user = serializer.is_valid(raise_exception=True)
        status="Your password is changed."
        return Response({'status': status})

class VerificationView(views.APIView):
    serializer_class = Verification
    def get(self, request, *args, **kwargs):
        uid = kwargs.get('id')
        hash_pass = kwargs.get('hash_pass')
        context = { 'id' : uid, 'hash_pass' : hash_pass}
        serializer = self.serializer_class(data=request.data, context=context)
        user = serializer.is_valid(raise_exception=True)
        status="Your account is verified."
        #return Response({'status': status})
        return HttpResponseRedirect("/verified")
        
class ObtainAuthToken(views.APIView):
    throttle_classes = ()
    permission_classes = ()
    authentication_classes = (TokenAuthentication, )
    # parser_classes = (parsers.FormParser, parsers.MultiPartParser, parsers.JSONParser,)
    # renderer_classes = (renderers.JSONRenderer,)
    serializer_class = AuthTokenSerializer

    def post(self, request, *args, **kwargs):
        serializer = self.serializer_class(data=request.data)
        serializer.is_valid(raise_exception=True)
        user = serializer.validated_data['user']
        token, created = Token.objects.get_or_create(user=user)
        return Response({'token': token.key})


class ForgotPasswordVerifyView(views.APIView):
    serializer_class = ForgotPasswordVerify
    def get(self, request, *args, **kwargs):
        code = kwargs.get('code')
        context = { 'code' : code}
        serializer = self.serializer_class(data=request.data, context=context)
        user = serializer.is_valid(raise_exception=True)
        status="Your account is verified."
        #return Response({'status': status})
        return HttpResponseRedirect("/forgetverified")


class ActiveUserVerifyView(views.APIView):
    def get(self, request, *args, **kwargs):
        email = kwargs.get('email')
        user = User.objects.get(email__iexact=email)
        return Response({'response': user.is_active})
        
        
obtain_auth_token = ObtainAuthToken.as_view()
register_view = RegistrationView.as_view()
