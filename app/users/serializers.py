from django.contrib.auth import authenticate
from django.utils.translation import ugettext_lazy as _

from rest_framework import serializers
from rest_framework_mongoengine.serializers import DocumentSerializer
import smtplib
from users.models import *
from license.models import *
from parental_control.models import *
from router_inventory.models import *
from email.mime.multipart import MIMEMultipart
from email.mime.text import MIMEText
from django.template.loader import render_to_string
from django.conf import settings
import hashlib
import datetime


class AuthTokenSerializer(serializers.Serializer):
    email = serializers.EmailField(label=_("Email"))
    password = serializers.CharField(label=_("Password"), style={'input_type': 'password'})

    def validate(self, attrs):
        username = attrs.get('email')
        password = attrs.get('password')
        print(User.objects.filter(email__iexact=username))
        if username and password:
            user = authenticate(username=username.lower(), password=password)

            if user:
                joiningdate = user.date_joined
                end_date = joiningdate + datetime.timedelta(days=30)
                end_date = end_date.date()
                CurrentDate = datetime.datetime.now().date()
                #if end_date <= CurrentDate and not user.oldcode:
                #    msg = _('Your invite code is expire.')
                #    raise serializers.ValidationError(msg)
                # From Django 1.10 onwards the `authenticate` call simply
                # returns `None` for is_active=False users.
                # (Assuming the default `ModelBackend` authentication backend.)
                if not user.is_active:
                    msg = _('This account is not verified yet.')
                    raise serializers.ValidationError(msg)
            else:
                msg = _('Unable to log in with provided credentials.')
                raise serializers.ValidationError(msg)
        else:
            msg = _('Must include "username" and "password".')
            raise serializers.ValidationError(msg)

        attrs['user'] = user
        return attrs

class Registration(serializers.Serializer):
    email = serializers.EmailField(label=_("Email"))
    password = serializers.CharField(label=_("Password"), style={'input_type': 'password'})
    product_key = serializers.CharField(label=_("Product Key"))
    mac_id = serializers.CharField(label=_("MAC ID"))
    router_ip = serializers.CharField(label=_("Router IP"))
    def validate(self, attrs):
        email = attrs.get('email')
        password = attrs.get('password')
        mac_id = attrs.get('mac_id').upper()
        router_ip = attrs.get('router_ip')
        product_key = attrs.get('product_key')
        if password and email:
            user = User.objects.filter(email__iexact=email)
            if user:
                msg = _('This email already exists.')
                raise serializers.ValidationError(msg)
            else:
                
                serial_number = RouterInventory.objects.filter(serial_number__iexact=product_key)
                if not serial_number:
                    msg = _('The product Key is not valid')
                    raise serializers.ValidationError(msg)
                elif serial_number[0].router_status != "Unallocated":
                    msg = _('The product Key is already alloted')
                    raise serializers.ValidationError(msg)
                elif serial_number[0].router_macid != mac_id:
                    msg = _('Invalid Router MAC ID, This key is assigned to some other router')
                    raise serializers.ValidationError(msg)
                else:
                
                    allusers = User.objects.all().order_by("-id")
                    user_id = 1
                    if allusers:
                        getid = allusers[0].id
                        user_id = getid + 1
                    new_user = User(
                        id=user_id,
                        username=attrs.get('email').lower(),
                        email=attrs.get('email').lower(),
                        name=attrs.get('email').lower(),
                        mac_id=attrs.get('mac_id'),
                        router_ip = attrs.get('router_ip'),
                        product_key=attrs.get('product_key'),
                        is_active=True,
                        is_staff=False
                    )
                    new_user.set_password(password)
                    new_user.save()
                    hash_pass = hashlib.sha1(password.encode('utf-8'))
                    hash_pass = hash_pass.hexdigest()
                    verify = VerificationData(
                    passw=hash_pass,
                    user_id=user_id
                    )
                    verify.save()
                    #serial_number[0].set_status("active")
                    
                    fromaddr = settings.USER
                    pwd = settings.PASSWORD
                    msg = MIMEMultipart()
                    msg['From'] = settings.EMAIL_ADDRESS
                    recipients = [email]
                    msg['To'] = ", ".join(recipients)
    
                    msg['Subject'] = "Welcome to Xbrain"
    
                    content_html = render_to_string("verification.html", {"id":user_id,"hash_pass":hash_pass})
                    #content_html = content_html.encode('utf-8').strip()
    
                    test = MIMEText(content_html, 'html')
                    msg.attach(test)
                    print("sending email")
                    #server = smtplib.SMTP('smtp.gmail.com', 587)
                    server = smtplib.SMTP("e26.ehosts.com", 587)
                    server.ehlo()
                    server.starttls()
                    server.login(fromaddr, pwd)
                    text = msg.as_string()
    
                    server.sendmail(settings.EMAIL_ADDRESS, recipients, text)
                    server.quit()
                    pincode = Pincode(router_macid=mac_id,router_serial_number=product_key)
                    #pincode = Pincode(router_macid=mac_id)
                    pincode.save()
        else:
            msg = _('Must include "Email" and "password".')
            raise serializers.ValidationError(msg)

        attrs['user'] = user
        return attrs

class ForgetPassword(serializers.Serializer):
    email = serializers.EmailField(label=_("Email"))
    def validate(self, attrs):
        email = attrs.get('email')
        if email:
            user = User.objects.filter(email__iexact=str(email))
            if not user:
                msg = _('This email does not exists.')
                raise serializers.ValidationError(msg)
            else:
                code = binascii.hexlify(os.urandom(20)).decode()
                entry = ForgetPass(user=user[0].id, code=code)
                entry.save()
                print(entry.code)
                fromaddr = settings.EMAIL_ADDRESS
                pwd = settings.PASSWORD
                msg = MIMEMultipart()
                msg['From'] = settings.EMAIL_ADDRESS
                recipients = [email]
                msg['To'] = ", ".join(recipients)

                msg['Subject'] = "Forget Password"

                content_html = render_to_string("forget.html", {"code":code})
                content_html = content_html

                test = MIMEText(content_html, 'html')
                msg.attach(test)

                server = smtplib.SMTP('e26.ehosts.com', 587)
                server.starttls()
                server.login(fromaddr, pwd)

                text = msg.as_string()

                server.sendmail(fromaddr, recipients, text)
                server.quit()
        else:
            msg = _('Must include "Email".')
            raise serializers.ValidationError(msg)

        attrs['user'] = user
        return attrs

class ForgotPasswordVerify(serializers.Serializer):
    def validate(self, attrs):
        encode_pass = self.context['code']
        if encode_pass:
            verification = ForgetPass.objects.filter(code=encode_pass)
            if not verification:
                msg = _('verification failedddd.'+verification)
                raise serializers.ValidationError(msg)
            if verification[0].created + datetime.timedelta(minutes=10) < datetime.datetime.now():
                msg = _('Verification link expired.')
                raise serializers.ValidationError(msg)
            else:
                user = User.objects.get(email__iexact=verification[0].user.email)
                user.is_active=False
                user.save()
        else:
            msg = _('Must include the code')
            raise serializers.ValidationError(msg)

        return attrs
        
class UpdateCode(serializers.Serializer):
    email = serializers.EmailField(label=_("Email"))
    password = serializers.CharField(label=_("Password"), style={'input_type': 'password'})
    newcode = serializers.CharField(label=_("New Code"))
    def validate(self, attrs):
        email = attrs.get('email')
        password = attrs.get('password')
        newcode = attrs.get('newcode')
        if email and password and newcode:
            user = authenticate(username=email.lower(), password=password)
            if not user:
                msg = _('Invalid email or password.')
                raise serializers.ValidationError(msg)
            else:
                old_code = user.product_key
                userdetails = User.objects.get(email__iexact=email)
                userdetails.product_key = newcode
                userdetails.oldcode = old_code
                userdetails.save()
        else:
            msg = _('Must include "Email", "Password", "New Code".')
            raise serializers.ValidationError(msg)

        attrs['user'] = user
        return attrs

class ResetPassword(serializers.Serializer):
    email = serializers.EmailField(label=_("Email"))
    new_password = serializers.CharField(label=_("New Password"), style={'input_type': 'password'})
    def validate(self, attrs):
        email = attrs.get('email')
        new_password = attrs.get('new_password')
        if new_password:
            user = User.objects.filter(email__iexact=email)
            if not user:
                msg = _('email is invalid..')
                raise serializers.ValidationError(msg)
            else:
                user = user[0]
                user.set_password(attrs.get('new_password'))
                user.is_active = True
                user.save()
        else:
            msg = _('Must include "Password".')
            raise serializers.ValidationError(msg)

        attrs['user'] = user
        return attrs
        

class ChangePassword(serializers.Serializer):
    old_password = serializers.CharField(label=_("Old Password"), style={'input_type': 'password'})
    new_password = serializers.CharField(label=_("New Password"), style={'input_type': 'password'})
    token = serializers.CharField(label=_("Token"))
    def validate(self, attrs):
        old_password = attrs.get('old_password')
        new_password = attrs.get('new_password')
        token = attrs.get('token')
        if new_password:
            tokendetails = Token.objects.get(key=token)
            if not tokendetails:
                msg = _('Invalid token.')
                raise serializers.ValidationError(msg)
            else:
                
                newuserdetails = tokendetails.user
                if newuserdetails.check_password(old_password):
                    newuserdetails.set_password(attrs.get('new_password'))
                    newuserdetails.save()
                else:
                    msg = _('Invalid old password')
                    raise serializers.ValidationError(msg)
        else:
            msg = _('Must include "Password".')
            raise serializers.ValidationError(msg)

        attrs['user'] = tokendetails.user
        return attrs

class Verification(serializers.Serializer):
    def validate(self, attrs):
        userid = self.context['id']
        encode_pass = self.context['hash_pass']
        if userid and encode_pass:
            verification = VerificationData.objects.filter(user_id=userid,passw=encode_pass)
            if not verification:
                msg = _('verificationnnn failed.')
                raise serializers.ValidationError(msg)
            else:
                user = User.objects.get(id=userid)
                user.is_active=True
                user.save()
        else:
            msg = _('Must include "ID" and "Hash Password".')
            raise serializers.ValidationError(msg)

        return attrs
