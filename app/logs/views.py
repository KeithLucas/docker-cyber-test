from rest_framework import views, mixins, permissions, exceptions
from rest_framework.response import Response
from rest_framework_mongoengine import viewsets
from rest_framework import parsers, renderers

from .serializers import *
from users.models import *
from .models import *
from users.authentication import TokenAuthentication

from django.core import serializers
from django.http import JsonResponse

import json

class SaveAttackLogsView(views.APIView):
    serializer_class = SaveAttackLogs
    def post(self, request, *args, **kwargs):
        serializer = self.serializer_class(data=request.data)
        log = serializer.is_valid(raise_exception=True)
        status="Your log is saved"
        return Response({'status': status})

class GetAttackLogsView(views.APIView):
    serializer_class = GetAttackLogs
    def post(self, request, *args, **kwargs):
        serializer = self.serializer_class(data=request.data)
        log = serializer.is_valid(raise_exception=True)
        if log:
            till_date = serializer.data['till_date']
            if till_date == "":
              	till_date = datetime.datetime.now().isoformat().split(".")[0]+'Z'
            
            print(till_date)
            row = AttackLog.objects.all().filter(attack_date__gte=serializer.data['from_date'],
                                            attack_date__lte=till_date, 
                                            router_macid__iexact=serializer.data['router_macid'])
            #row = AttackLog.objects.filter(router_macid__iexact=serializer.data['router_macid'])
            len(row)
            rows = row.to_json()
            print(len(row))
        else:
            rows = "Failed to get data"
        status="Your result is here"
        return Response({'status': status, 'count':len(row), 'rows':json.loads(rows)})
