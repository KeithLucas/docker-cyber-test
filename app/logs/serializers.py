from django.contrib.auth import authenticate
from django.utils.translation import ugettext_lazy as _

from rest_framework import serializers
from rest_framework_mongoengine.serializers import DocumentSerializer
from logs.models import *
from users.models import *
from django.conf import settings
import datetime

class SaveAttackLogs(serializers.Serializer):
    router_macid = serializers.CharField(label=_("Router MAC ID"))
    token = serializers.CharField(label=_("Token"))
    sig_name=serializers.CharField(label=_('Signature Name'))
    sig_class=serializers.CharField(label=_('Signature Class'))
    sig_id = serializers.CharField(label=_('Signature ID'))
    priority = serializers.CharField(label=_('Priority'))
    src_ip = serializers.CharField(label=_('Source IP'))
    src_port = serializers.CharField(label=_('Source Port'))
    device_ip = serializers.CharField(label=_('Device IP'))
    device_name = serializers.CharField(label=_('Device Name'))
    device_os = serializers.CharField(label=_('Device OS'))
    device_macid = serializers.CharField(label=_('Device MAC ID'))
    dest_port = serializers.CharField(label=_('Device Port'))
    intrusion_url = serializers.CharField(label=_('Intrusion URL'))
    user_agent = serializers.CharField(label=_('User Agent'))
    conn_type = serializers.CharField(label=_('Connection Type'))
    connected_devices = serializers.IntegerField(label=_('Number of connected Devices'))
    attack_date = serializers.CharField(label=_('Date of attack'))

    def validate(self, attrs):
        router_macid = attrs.get('router_macid')
        token = attrs.get('token')
        if router_macid:
            tokendetails = validate_token(token)
            if not tokendetails:
                msg = _('Invalid token.')
                raise serializers.ValidationError(msg)
            else:
                
                userdetails = tokendetails.user
                if userdetails.check_mac_id(router_macid):
                    all_logs = AttackLog.objects.all().order_by("-id")
                    log_id = 1
                    
                    if all_logs:
                        getid = all_logs[0].id
                        log_id = getid + 1
                        
                    new_log = AttackLog(
                        id=log_id,
                        router_macid=attrs.get('router_macid').upper(),
                        sig_name=attrs.get('sig_name'),
                        sig_class=attrs.get('sig_class'),
                        sig_id=attrs.get('sig_id'),
                        priority=attrs.get('priority'),
                        src_ip=attrs.get('src_ip'),
                        src_port=attrs.get('src_port'),
                        device_ip=attrs.get('device_ip'),
                        device_name=attrs.get('device_name'),
                        device_os=attrs.get('device_os'),
                        device_macid=attrs.get('device_macid').upper(),
                        dest_port=attrs.get('dest_port'),
                        intrusion_url=attrs.get('intrusion_url'),
                        user_agent=attrs.get('user_agent'),
                        conn_type=attrs.get('conn_type'),
                        connected_devices=attrs.get('connected_devices'),
                        attack_date=attrs.get('attack_date')
                    )
                    new_log.save()
                    
                    print("saving log",log_id)
                else:
                    msg = _('Invalid MAC ID')
                    raise serializers.ValidationError(msg)
        else:
            msg = _('Must include "MAC ID".')
            raise serializers.ValidationError(msg)

        attrs['mac_id'] = userdetails.mac_id
        return attrs
        
        
def validate_token(token):
    try:
        return Token.objects.get(key=token)
    except:
        return
        
class GetAttackLogs(serializers.Serializer):
    router_macid = serializers.CharField(label=_("Router MAC ID"))
    token = serializers.CharField(label=_("Token"))
    from_date = serializers.CharField(label=_('From date'), default="")
    till_date = serializers.CharField(label=_('Till date'), default="")
    def validate(self, attrs):
        router_macid = attrs.get('router_macid')
        token = attrs.get('token')
        from_date = attrs.get('from_date')
        till_date = attrs.get('till_date')
        if till_date == "":
        	till_date = datetime.datetime.now().isoformat().split(".")[0]+'Z'

        if router_macid:
            tokendetails = validate_token(token)
            if not tokendetails:
                msg = _('Invalid token.')
                raise serializers.ValidationError(msg)
            else:
                userdetails = tokendetails.user
                if userdetails.check_mac_id(router_macid):
                    rows = AttackLog.objects.all().filter(router_macid__iexact=router_macid,attack_date__gte=from_date, attack_date__lte = till_date)
                    print(len(rows))
                    
                else:
                    msg = _('Invalid MAC ID')
                    raise serializers.ValidationError(msg)
        else:
            msg = _('Must include "MAC ID".')
            raise serializers.ValidationError(msg)

        attrs['rows'] = rows
        return attrs
