import datetime
import binascii
import os

from django.db import models
from django.utils import timezone
from django.utils.encoding import python_2_unicode_compatible
from django.utils.translation import ugettext_lazy as _

import mongoengine
from mongoengine.django import auth
from mongoengine import fields, Document, ImproperlyConfigured

class AttackLog(Document):
    
    id = fields.IntField(primary_key=True)
    router_macid = fields.StringField(required=True)
    sig_name = fields.StringField()
    sig_class = fields.StringField()
    sig_id = fields.StringField()
    priority = fields.StringField()
    src_ip = fields.StringField()
    src_port = fields.StringField()
    device_ip = fields.StringField()
    device_name = fields.StringField()
    device_os = fields.StringField()
    device_macid = fields.StringField()
    dest_port = fields.StringField()
    intrusion_url = fields.StringField()
    user_agent = fields.StringField()
    conn_type = fields.StringField()
    connected_devices = fields.IntField()
    created_date = fields.DateTimeField(default=timezone.now, verbose_name=_('date joined'))
    attack_date = fields.StringField()
    
    REQUIRED_FIELDS = ['router_macid']
    
    @classmethod
    def create_log(cls, router_macid, sig_name, sig_class, sig_id, priority, src_ip, src_port, device_ip, device_name, device_os, device_macid,
                   dest_port, intrusion_url, user_agent, conn_type, connected_devices, attack_date):
        """
        Create (and save) a new log with the given details
        """
        now = datetime.datetime.now()
        log = cls(router_macid=router_macid, sig_name=sig_name, sig_class=sig_class, sig_id=sig_id, priority=priority, src_ip=src_ip,
                   src_port=src_port, device_ip=device_ip, device_name=device_name, device_os=device_os, device_macid=device_macid, dest_port=dest_port,
                   intrusion_url=intrusion_url, user_agent=user_agent, conn_type=conn_type, connected_devices=connected_devices,
                   attack_date=attack_date, created_date=now)
        log.save()
        return log
        
