from django.apps import AppConfig


class RouterInventoryConfig(AppConfig):
    name = 'router_inventory'
