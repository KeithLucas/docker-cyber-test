from datetime import datetime, timedelta
import binascii
import os

from django.db import models
from django.utils import timezone
from django.utils.encoding import python_2_unicode_compatible
from django.utils.translation import ugettext_lazy as _

import mongoengine
from mongoengine.django import auth
from mongoengine import fields, Document, ImproperlyConfigured

class RouterInventory(Document):
    
    id = fields.IntField(primary_key=True)
    serial_number = fields.StringField(required=True)
    router_macid = fields.StringField(required=True)
    modified_date = fields.DateTimeField(default=datetime.now())
    router_status = fields.StringField(default='Unallocated')
    
    REQUIRED_FIELDS = ['serial_number','router_macid']
    
    @classmethod
    def create_entry(cls, serial_number, router_macid, router_status):
        """
        Create (and save) a new key entry in DB
        """
        now = datetime.now()
        entry = cls(serial_number=serial_number, router_macid=router_macid, router_status='Unallocated', modified_date=now)
        entry.save()
        return entry
        
    def set_status(self, status):
        
        self.router_status = status
        self.save()
        return self