from rest_framework import views, mixins, permissions, exceptions
from rest_framework.response import Response
from rest_framework_mongoengine import viewsets
from rest_framework import parsers, renderers
from users.authentication import TokenAuthentication

from django.core import serializers
from django.http import JsonResponse

import json
from router_inventory.models import *
from users.models import *

def validate_token(token):
    try:
        return Token.objects.get(key=token)
    except:
        return
    
class AddToRouterInventoryView(views.APIView):
    #serializer_class = UpdateDeviceConnections
    def post(self, request, *args, **kwargs):
        data = request.data
        
        if data.get('router_macid') and data.get('serial_number'):
            tokendetails = validate_token(data.get('token'))
            if not tokendetails:
                status = 'Invalid token'
            else:
                userdetails = tokendetails.user
                if userdetails.check_staff():
                    row = RouterInventory.objects.filter(serial_number__iexact=data.get('serial_number'))
                    if not row:
                        all_entries = RouterInventory.objects.all().order_by("-id")
                        entry_id = 1
                        
                        if all_entries:
                            getid = all_entries[0].id
                            entry_id = getid + 1
                        
                        new_entry = RouterInventory(
                            id=entry_id,
                            serial_number= data.get('serial_number').upper(),
                            router_macid= data.get('router_macid').upper()
                            )
                        
                        new_entry.save()
                        status="Router details are added"
                    else:
                        status="Router details are already in Database"
                else:
                    status = 'User not authorized for this'
        else:
            status = 'Must include both, MAC ID and serial number of router.'
            
        return Response({'status': status})
        