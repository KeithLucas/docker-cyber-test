from __future__ import unicode_literals

from django.template.response import TemplateResponse

from rest_framework_mongoengine.viewsets import ModelViewSet as MongoModelViewSet



def index_view(request):
    context = {}
    return TemplateResponse(request, 'index.html', context)
