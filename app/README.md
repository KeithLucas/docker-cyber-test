Run below commands in order to install dependencies:

sudo apt-get install python-pip pip install django==1.9 djangorestframework==3.3.3 mongoengine==0.9 pymongo==2.7 django-rest-framework-mongoengine

Once above is done, run below command inside backend folder,

python3 manage.py runserver <IP>:<PORT>

Where IP and PORT are desired optional variables. If they are absent, server will start at 0.0.0.0:3642

screen

python3 manage.py runserver 0.0.0.0:3642

backup db : mongodump --db backend -u xbrainserver -p XbrainTesting --out databse_backup