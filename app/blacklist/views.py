from rest_framework import views, mixins, permissions, exceptions
from rest_framework.response import Response
from rest_framework_mongoengine import viewsets
from rest_framework import parsers, renderers

from .serializers import *
from users.models import *
from .models import *
from users.authentication import TokenAuthentication

from django.core import serializers
from django.http import JsonResponse

import json

class AddToBlackListView(views.APIView):
    serializer_class = AddToBlackList
    def post(self, request, *args, **kwargs):
        serializer = self.serializer_class(data=request.data)
        log = serializer.is_valid(raise_exception=True)
        status="Your entry is saved"
        return Response({'status': status})

class RemoveFromBlackListView(views.APIView):
    serializer_class = RemoveFromBlackList
    def post(self, request, *args, **kwargs):
        serializer = self.serializer_class(data=request.data)
        log = serializer.is_valid(raise_exception=True)
        status="Status of given IP/domain is changed to allowed"
        return Response({'status': status})


class GetBlackListView(views.APIView):
    serializer_class = GetBlackList
    def post(self, request, *args, **kwargs):
        serializer = self.serializer_class(data=request.data)
        log = serializer.is_valid(raise_exception=True)
        if log:
            rows = BlackList.objects.filter(router_macid__iexact=serializer.data['router_macid'], block_status=1).only('block_ip_domain')
            blacklist = [row.block_ip_domain for row in rows]
        else:
            rows = "Failed to get data"
        status="Here is the list of blocked IPs and domains"
        return Response({'status': status, 'blacklist': blacklist })