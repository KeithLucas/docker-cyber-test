from django.contrib.auth import authenticate
from django.utils.translation import ugettext_lazy as _

from rest_framework import serializers
from rest_framework_mongoengine.serializers import DocumentSerializer
from blacklist.models import *
from users.models import *
from django.conf import settings
import datetime

class AddToBlackList(serializers.Serializer):
    router_ip = serializers.CharField(label=_("Router IP"))
    router_macid = serializers.CharField(label=_("Router MAC ID"))
    token = serializers.CharField(label=_("Token"))
    block_ip_domain=serializers.CharField(label=_('Block name'))
    
    def validate(self, attrs):
        router_ip = attrs.get('router_ip')
        router_macid = attrs.get('router_macid')
        token = attrs.get('token')
        block_ip_domain = attrs.get('block_ip_domain')
        if router_macid:
            tokendetails = validate_token(token)
            if not tokendetails:
                msg = _('Invalid token.')
                raise serializers.ValidationError(msg)
            else:
                userdetails = tokendetails.user
                if userdetails.check_mac_id(router_macid):
                    entry = BlackList()
                    if entry.check_entry(router_macid=router_macid,block_ip_domain=block_ip_domain):
                        entry.update_entry(router_macid=router_macid,block_ip_domain=block_ip_domain, status=1)
                    else:
                        all_entries = BlackList.objects.all().order_by("-id")
                        entry_id = 1
                        
                        if all_entries:
                            getid = all_entries[0].id
                            entry_id = getid + 1
                            
                        new_entry = BlackList(
                            id=entry_id,
                            router_ip=attrs.get('router_ip'),
                            router_macid = attrs.get('router_macid').upper(),
                            block_ip_domain=attrs.get('block_ip_domain'),
                            
                        )
                        new_entry.save()
                else:
                    msg = _('Invalid MAC ID')
                    raise serializers.ValidationError(msg)
        else:
            msg = _('Must include "MAC ID".')
            raise serializers.ValidationError(msg)

        attrs['mac_id'] = userdetails.mac_id
        return attrs
        

class RemoveFromBlackList(serializers.Serializer):
    router_ip = serializers.CharField(label=_("Router IP"))
    router_macid = serializers.CharField(label=_("Router MAC ID"))
    token = serializers.CharField(label=_("Token"))
    block_ip_domain=serializers.CharField(label=_('Block name'))
    
    def validate(self, attrs):
        router_ip = attrs.get('router_ip')
        router_macid = attrs.get('router_macid')
        token = attrs.get('token')
        block_ip_domain = attrs.get('block_ip_domain')
        if router_macid:
            tokendetails = validate_token(token)
            if not tokendetails:
                msg = _('Invalid token.')
                raise serializers.ValidationError(msg)
            else:
                userdetails = tokendetails.user
                if userdetails.check_mac_id(router_macid):
                    entry = BlackList()
                    if entry.check_entry(router_macid=router_macid,block_ip_domain=block_ip_domain):
                        entry.update_entry(router_macid=router_macid,block_ip_domain=block_ip_domain, status=0)
                    else:
                        msg = _('Respective IP/Domain is not in blocked list')
                        raise serializers.ValidationError(msg)
                else:
                    msg = _('Invalid MAC ID')
                    raise serializers.ValidationError(msg)
        else:
            msg = _('Must include "MAC ID".')
            raise serializers.ValidationError(msg)

        attrs['mac_id'] = userdetails.mac_id
        return attrs
        
        
def validate_token(token):
    try:
        return Token.objects.get(key=token)
    except:
        return
        
class GetBlackList(serializers.Serializer):
    router_macid = serializers.CharField(label=_("Router MAC ID"))
    token = serializers.CharField(label=_("Token"))
    def validate(self, attrs):
        router_macid = attrs.get('router_macid')
        token = attrs.get('token')
        if router_macid:
            tokendetails = validate_token(token)
            if not tokendetails:
                msg = _('Invalid token.')
                raise serializers.ValidationError(msg)
            else:
                userdetails = tokendetails.user
                if userdetails.check_mac_id(router_macid):
                    rows = BlackList.objects.filter(router_macid__iexact=router_macid, block_status=1)
                    #print(rows)
                else:
                    msg = _('Invalid MAC ID')
                    raise serializers.ValidationError(msg)
        else:
            msg = _('Must include "MAC ID".')
            raise serializers.ValidationError(msg)

        attrs['rows'] = rows
        return attrs