import datetime
import binascii
import os

from django.db import models
from django.utils import timezone
from django.utils.encoding import python_2_unicode_compatible
from django.utils.translation import ugettext_lazy as _

import mongoengine
from mongoengine.django import auth
from mongoengine import fields, Document, ImproperlyConfigured

class BlackList(Document):
    
    id = fields.IntField(primary_key=True)
    router_ip = fields.StringField(required=True)
    router_macid = fields.StringField(required=True)
    block_ip_domain = fields.StringField()
    block_status = fields.IntField(default=1)
    modified_date = fields.DateTimeField(default=timezone.now, verbose_name=_('date created'))
    
    REQUIRED_FIELDS = ['router_ip', 'router_macid', 'block_ip_domain']
    
    @classmethod
    def create_entry(cls, router_ip, router_macid, block_ip_domain):
        """
        Create (and save) a new log with the given details
        """
        now = datetime.datetime.now()
        log = cls(router_ip=router_ip, router_macid=router_macid, block_ip_domain=block_ip_domain, created_date=now, block_status=1)
        log.save()
        return log
        
    def check_entry(self, router_macid, block_ip_domain):
        """
        Check if same entry already exist in database
        """
        
        try:
            entry = BlackList.objects.get(router_macid__iexact=router_macid,block_ip_domain=block_ip_domain)
            return True
        except:
            return False
        
    def update_entry(self, router_macid, block_ip_domain, status):
        """
        Update if same entry already exist in database
        """
        
        try:
            entry = BlackList.objects.get(router_macid__iexact=router_macid,block_ip_domain=block_ip_domain)
            entry.block_status = status
            entry.modified_date = datetime.datetime.now()
            entry.save()
        except:
            return