from django.conf import settings
from django.conf.urls.static import static
from django.conf.urls import include, url
from django.contrib import admin

from users.views import *
from app.views import *
from logs.views import *
from blacklist.views import *
from license.views import *
from signature_updates.views import *
from parental_control.views import *
from router_inventory.views import *
from backend.routers import HybridRouter
from django.views.generic import TemplateView

# We use a single global DRF Router that routes views from all apps in project
router = HybridRouter()

router.add_api_view(r'auth', url(r'^auth$', ObtainAuthToken.as_view(), name=r"auth"))


urlpatterns = [
    # default django admin interface (currently unused)
    url(r'^admin/', include(admin.site.urls)),

    # root view of our REST api, generated by Django REST Framework's router
    url(r'^api/', include(router.urls, namespace='api')),
    url(r'^login$', ObtainAuthToken.as_view(), name=r"login"),
    # index page should be served by django to set cookies, headers etc.
    url(r'^$', index_view, {}, name='index'),
    url(r'^createuser$', RegistrationView.as_view(), name=r"registration"),
    url(r'^forgetpassword$', ForgetPasswordView.as_view(), name=r"forget"),
    url(r'^forgetpassword_validate/(?P<code>[0-9a-z]+)$', ForgotPasswordVerifyView.as_view(), name=r"forgetverify"),
    url(r'^forgetverified$',TemplateView.as_view(template_name="ForgetPassword.html"),name=r"Verified Forget Password"),
    #3b0073810ccf0b648508da00bcc2363e.html
    url(r'^activeuserverify/(?P<email>[0-9a-z@.]+)$', ActiveUserVerifyView.as_view(), name=r"active user verify"),
    url(r'^resetpassword$', ResetPasswordView.as_view(), name=r"reset"),
    url(r'^changepassword$', ChangePasswordView.as_view(), name=r"change"),
    url(r'^update/invite/code$', NewCode.as_view(), name=r"invite_code"),
    #url(r'^verification$', VerificationView.as_view(), name=r"verification"),
    url(r'^verified$',TemplateView.as_view(template_name="VerificationDone.html"),name=r"Verified User"),
    url(r'^login_validate/(?P<id>[0-9]+)/(?P<hash_pass>[0-9a-z]+)$', VerificationView.as_view(), name=r"verification"),
    url(r'^createlog', SaveAttackLogsView.as_view(), name=r"createlog"),
    url(r'^getlog', GetAttackLogsView.as_view(), name=r"getlog"),
    url(r'^addtoblacklist', AddToBlackListView.as_view(), name=r"addtoblacklist"),
    url(r'^removefromblacklist', RemoveFromBlackListView.as_view(), name=r"removefromblacklist"),
    url(r'^getblacklist', GetBlackListView.as_view(), name=r"getblacklist"),
    url(r'^addkeystodb', AddKeysView, name=r"addkeystodb"),
    url(r'^keysadded',TemplateView.as_view(template_name="keysadded.html"),name=r"New Keys added"),
    url(r'^uploadsignature', uploadsignature, name='uploadsignature'),
    url(r'^updatedeviceconnection', UpdateDeviceConnectionsView.as_view(), name=r"updatedeviceconnection"),
    url(r'^addupdatedevicestate', AddUpdateDeviceStateView.as_view(), name=r"addupdatedevicestate"),
    url(r'^getconnecteddevices', GetConnectedDevicesView.as_view(), name=r"getconnecteddevices"),
    url(r'^addupdateblocklist', AddUpdateBlockListView.as_view(), name=r"addupdateblocklist"),
    url(r'^getdeviceblocklist', GetDeviceBlockListView.as_view(), name=r"getdeviceblocklist"),
    url(r'^addnewrouter', AddToRouterInventoryView.as_view(), name=r"addtorouterinventory"),
    url(r'^verifyupdatepin', VerifyUpdatePincodeView.as_view(), name=r"verifyupdatepin"),
    url(r'^3b0073810ccf0b648508da00bcc2363e.html',TemplateView.as_view(template_name="3b0073810ccf0b648508da00bcc2363e.html"),name=r"cert"),
]

# let django built-in server serve static and media content
urlpatterns += static(settings.STATIC_URL, document_root=settings.STATIC_ROOT)
urlpatterns += static(settings.MEDIA_URL, document_root=settings.MEDIA_ROOT)
